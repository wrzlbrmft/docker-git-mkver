#!/usr/bin/env sh
GITHUB_REPOSITORY_URL="https://github.com/mikefarah/yq"

LATEST_RELEASE_TAG="$(./get-latest-release-tag.sh "${GITHUB_REPOSITORY_URL}")"
LATEST_VERSION="$(echo "${LATEST_RELEASE_TAG}" | awk -F'^v?' '{print $2}')"

echo "yq: LATEST_VERSION=${LATEST_VERSION}"
URL="https://github.com/mikefarah/yq/releases/download/v${LATEST_VERSION}/yq_linux_amd64.tar.gz"
echo "yq: URL=${URL}"
curl -sL "${URL}" | tar zxv --no-same-owner -C "$(pwd)"
rm -fv yq
mv -v yq_* yq

test -f "yq"
