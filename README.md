# docker-git-mkver

Automatic Semantic Versioning for git based software development for CI/CD pipelines in Docker

```
docker pull wrzlbrmft/git-mkver:latest
```

See also:

  * https://idc101.github.io/git-mkver/
  * https://github.com/idc101/git-mkver
  * https://hub.docker.com/r/wrzlbrmft/git-mkver/tags

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
